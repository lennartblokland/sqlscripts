SELECT Artiest, SUM(Aantalbeluisteringen) "Specifiek aantal beluisteringen"
FROM liedjes
GROUP BY Artiest
HAVING SUM(Aantalbeluisteringen) IN (17,50,100);