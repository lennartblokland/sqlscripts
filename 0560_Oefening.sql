SET SQL_SAFE_UPDATES = 0;
UPDATE liedjes
SET Artiest_Id = 1
WHERE Artiest = "Led Zeppelin";
UPDATE liedjes
SET Artiest_Id = 2
WHERE Artiest = "The Doors";
UPDATE liedjes
SET Artiest_Id = 3
WHERE Artiest = "Molly Tuttle";
UPDATE liedjes
SET Artiest_Id = 4
WHERE Artiest = "Goodnight, Texas";
UPDATE liedjes
SET Artiest_Id = 5
WHERE Artiest = "Layla Zoe";
UPDATE liedjes
SET Artiest_Id = 6
WHERE Artiest = "Danielle Nicole";
UPDATE liedjes
SET Artiest_Id = 7
WHERE Artiest = "Van Halen";
SET SQL_SAFE_UPDATES = 1;