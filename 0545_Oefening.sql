SELECT Artiest, SUM(Aantalbeluisteringen) "Totaal aantal beluisteringen"
FROM liedjes
WHERE length(Artiest) >= 10
GROUP BY Artiest
HAVING SUM(Aantalbeluisteringen) > 100;