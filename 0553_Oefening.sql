SET SQL_SAFE_UPDATES = 0;
UPDATE baasjes
SET huisdieren_id = 1
WHERE naam = "Vincent";

UPDATE baasjes
SET huisdieren_id = 2
WHERE naam = "Christiane";

UPDATE baasjes
SET huisdieren_id = 3
WHERE naam = "Esther";

UPDATE baasjes
SET huisdieren_id = 4
WHERE naam = "Bert";

UPDATE baasjes
SET huisdieren_id = 5
WHERE naam = "Lyssa";

SET SQL_SAFE_UPDATES = 1;