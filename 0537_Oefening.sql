USE modernways;

SELECT voornaam, familienaam, COUNT(*) "Aantal boeken"
FROM boeken
GROUP BY voornaam, familienaam;