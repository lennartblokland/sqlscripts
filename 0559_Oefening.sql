ALTER TABLE liedjes
ADD COLUMN Artiest_Id INT,
ADD CONSTRAINT fk_liedjes_artiesten FOREIGN KEY (Artiest_Id) REFERENCES Artiesten(Artiest_Id);