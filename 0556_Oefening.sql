SELECT huisdieren.naam "Naam huisdier", baasjes.naam "Naam baasje"
from huisdieren
INNER JOIN baasjes ON huisdieren.Id = baasjes.huisdieren_id;