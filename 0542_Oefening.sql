SELECT Artiest, SUM(Aantalbeluisteringen) "Aantal beluisteringen"
FROM liedjes
GROUP BY Artiest
HAVING SUM(Aantalbeluisteringen) > 100;